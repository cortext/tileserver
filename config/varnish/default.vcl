vcl 4.1;

backend default {
  .host = "tileserver";
  .port = "8080";
}

/*
 * vcl_recv is executed when Varnish receives a request from the client.
 * Cache is bypassed if the cookie header is set in the request.
 * See:
 *   - https://www.varnish-software.com/developers/tutorials/varnish-builtin-vcl/#authorization-headers-and-cookies-are-not-cacheable
 *   - https://www.varnish-software.com/developers/tutorials/removing-cookies-varnish/
 */
sub vcl_recv {
    /*  remove cortextAccessToken cookie */
    set req.http.Cookie = regsuball(req.http.Cookie, "cortextAccessToken=[^;]+(; )?", "");
    /*  remove cookie header if there are no cookies left */
    if (req.http.Cookie ~ "^\s*$") {
        unset req.http.cookie;
    }
}
