#!/bin/sh

echo "STORAGE_PATH: $STORAGE_PATH"

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 <size_in_GB> <target_directory>"
  exit 1
fi

TARGET_DIR=$1
FONT_NAMES_FILE=$2

if ! command -v curl > /dev/null 2>&1 || ! command -v unzip > /dev/null 2>&1; then
  echo "Error: This script requires both 'curl' and 'unzip' to be installed."
  exit 1
fi

if [ ! -f "${FONT_NAMES_FILE}" ]; then
  echo "Error: Font names file ${FONT_NAMES_FILE} does not exist."
  exit 1
fi

BASE_URL="http://fonts.openmaptiles.org"

mkdir -p "${TARGET_DIR}"

while IFS= read -r FONT_NAME; do
    FILE="${FONT_NAME}.zip"
    # replace whitespace since curl needs encoded urls and font names contain whitespace
    URL="${BASE_URL}/$(echo "$FILE" | sed 's/ /%20/g')"

    if ! curl -s -f -o "${TARGET_DIR}"/"${FILE}" "${URL}"; then
      echo "Error: Failed to fetch ${URL}. Please try running:"
      echo "  curl -o '${TARGET_DIR}/${FILE}' '${URL}'"
      exit 1
    fi

    if ! unzip -o -q -d "${TARGET_DIR}" "${TARGET_DIR}"/"${FILE}"; then
      echo "Error: Failed to unzip ${TARGET_DIR}/${FILE}. Please check the file and try again."
      exit 1
    fi

    if ! chmod -R 755 "${TARGET_DIR}/${FONT_NAME}"; then
      echo "Error: Failed to set appropriate permissions on ${TARGET_DIR}/${FONT_NAME}. Please check the directory and try again."
      exit 1
    fi

    echo "${FONT_NAME} downloaded and unpacked"

    rm "${TARGET_DIR}/${FILE}"

done < "${FONT_NAMES_FILE}"

echo "All files have been successfully downloaded and unzipped to ${TARGET_DIR}."
