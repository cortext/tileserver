#!/bin/sh

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <size_in_GB> <target_directory>"
    exit 1
fi

SIZE=$1
TARGET_DIR=$2

mkdir -p "$TARGET_DIR"

FILE_NAME="${SIZE}G.bin"
FILE_PATH="$TARGET_DIR/$FILE_NAME"

if [ -f "$FILE_PATH" ]; then
    rm "$FILE_PATH"
fi

BLOCK_COUNT=$((SIZE * 1024))

dd if=/dev/zero of="$FILE_PATH" bs=1M count="$BLOCK_COUNT" status=progress

echo "File created: $FILE_PATH of size $SIZE GB"
