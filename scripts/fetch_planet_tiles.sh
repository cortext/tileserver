#!/bin/sh

echo "STORAGE_PATH: $STORAGE_PATH"

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
  echo "Usage: $0 <target_directory> <output_file_name> <mbtiles_file_url>"
  exit 1
fi

TARGET_DIR=$1
OUTFILE_NAME=$2
MBTILES_URL=$3

if ! command -v wget > /dev/null 2>&1; then
  echo "Error: This script requires 'wget'."
  exit 1
fi

mkdir -p "${TARGET_DIR}"

# Download the file with wget
echo
echo
echo "----------------------------------------------------------------"
echo "Beware the planet tiles are very large"
echo "You can interrupt the download with Ctrl+C and continue later..."
echo "----------------------------------------------------------------"
echo
echo

wget -c --progress=bar:force -O "${TARGET_DIR}/${OUTFILE_NAME}" "${MBTILES_URL}"


echo "Downloaded ${TARGET_DIR}/${OUTFILE_NAME}."
