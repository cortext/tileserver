# Tile Server with Varnish Cache

This repository provides configurations and scripts to deploy [TileServer GL](https://github.com/maptiler/tileserver-gl) with Varnish caching for efficient map tile delivery.

## Overview

The repository contains configurations for running TileServer GL with Varnish caching.

- `Makefile` automates setup and management tasks
- `docker-compose.yml` defines the TileServer and Varnish docker services
- `scripts/` contains setup scripts.
- `config/` contains config files for TileServer and varnish.
- `tileserver/styles` contains custom MapLibre GL styles for TileServer.

TileServer GL is run with custom styles located at [./tileserver/styles](./tileserver/styles). To include more styles, place them in [./tileserver/styles](./tileserver/styles) and modify [./config/tileserver/config.json](./config/tileserver/config.json) accordingly.

Varnish is run with the [file storage backend](https://varnish-cache.org/docs/7.4/users-guide/storage-backends.html#file), and configured to remove the `cortextAccessToken` cookie before handling the request. To configure varnish further, modify [./config/varnish/default.vcl](./config/varnish/default.vcl) or [./docker-compose.yml](./docker-compose.yml).

## Requirements

Setting up this project requires:

- make
- Docker and Docker Compose
- curl, wget and unzip

## Setup

1. Run `make init` to create the `.env` file.

2. Modify the `.env` file if needed:

- `STORAGE_PATH` is where the the fonts and the `.mbtiles` (around 80GB) files will be downloaded by the setup scripts.
- `CACHE_SIZE_GB` sets the cache file size in gigabytes. A file of this size will be generated during setup.
- `CACHE_PATH` is the directory where the cache file will be placed.
- `TILES_DOWNLOAD_URL` is the URL of the planet mbtiles file.
- `PORT` is the port that will be exposed in the host, where the tileserver will be available
- `TILESERVER_GL_VERSION` is the tag of the tileserver-gl image to use
- `VARNISH_INTERNAL_PORT` is the port that will be used by varnish in its container

3. Run `make setup` to generate the cache file and to download the vector font glyphs from [https://github.com/openmaptiles/fonts/](https://github.com/openmaptiles/fonts/)

4. Run `make fetch_planet_tiles` to start/resume downloading the file from `TILES_DOWNLOAD_URL`. The file will be placed at `STORAGE_PATH/mbtiles/planet.mbtiles`. If you already have a `planet.mbtiles` file available, you can place it there instead of downloading it, since it's quite large.

## Usage

Once the required files are set up:

- Run the tile server with `make up`.
- Access the tile server at `http://localhost:9000`.
- Check the tile server logs with `make logs`.
- Stop and remove the containers with `make down`.

## Other resources

Self-hosting map tiles this easily is possible thanks to the [OpenMapTiles project](https://github.com/openmaptiles/).

Planet mbtiles are generated and generously share with the community by [SenorKarlos](https://github.com/SenorKarlos/mbtiles) and [u/Holocrypto](https://www.reddit.com/r/openstreetmap/comments/v9yoed/downloadable_planet_europe_and_netherlands).

- TileServer GL configuration reference: https://maptiler-tileserver.readthedocs.io/en/latest/index.html
- Maputnik MapLibre GL styles editor: https://github.com/maplibre/maputnik
- Planet mbtiles sources
  - https://mbtiles.top/planet/Combined.mbtiles
  - https://osm.dbtc.link/mbtiles/2023-05-29-planet.mbtiles.lz4 (compressed with lz4)
- Varnish storage backends reference: https://varnish-cache.org/docs/7.4/users-guide/storage-backends.html
- Varnish built-in settings reference: https://www.varnish-software.com/developers/tutorials/varnish-builtin-vcl
- Removing cookies in varnish: https://www.varnish-software.com/developers/tutorials/removing-cookies-varnish/

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for enhancements, please open an issue or submit a pull request.

## Styles

The `cortext-base` custom GL style, located in [./tileserver/styles/cortext-base](./tileserver/styles/cortext-base) is derived from the [Positron](https://github.com/openmaptiles/positron-gl-style) and [OSM Bright](https://github.com/openmaptiles/osm-bright-gl-style) styles by OpenMapTiles.

# License

The code in this repository is available as free and open source software under the terms of the  [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text).
