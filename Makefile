ifneq ("$(wildcard .env)","")
	include .env
endif

STORAGE_PATH ?= ./tileserver
CACHE_PATH ?= ./cache
TILES_DOWNLOAD_URL ?= https://mbtiles.top/planet/Combined.mbtiles
CACHE_SIZE_GB ?= 1
PORT ?= 9000
TILESERVER_GL_VERSION ?= latest
VARNISH_INTERNAL_PORT ?= 80
export

init:
	@echo "Creating .env file from .env.defaults..."
	@envsubst < .env.defaults > .env
	@printf "\n.env file contents:\n\n"
	@cat .env
	@printf "\n\n"
	@echo "Make sure to modify the .env file according to your environment"
	@printf "\n\n"

setup:
	@if [ ! -f .env ]; then \
		echo ".env file not found. Please run 'make init' to create it."; \
		exit 1; \
	else \
		make fetch_fonts || { echo "Error: fetch_fonts failed"; exit 1; }; \
		make create_cache_file || { echo "Error: create_cache_file failed"; exit 1; }; \
		if [ ! -f $(STORAGE_PATH)/mbtiles/planet.mbtiles ]; then \
			echo "Run 'make fetch_planet_tiles' or place your planet.mbtiles file at $(STORAGE_PATH)/mbtiles"; \
		fi; \
	fi

create_cache_file:
	@echo "Creating cache file..."
	./scripts/create_cache_file.sh $(CACHE_SIZE_GB) $(CACHE_PATH)

fetch_planet_tiles:
	@echo "Downloading planet tiles..."
	./scripts/fetch_planet_tiles.sh $(STORAGE_PATH)/mbtiles planet.mbtiles $(TILES_URL)

fetch_fonts:
	@echo "Downloading and unpacking fonts..."
	./scripts/fetch_font_files.sh $(STORAGE_PATH)/fonts scripts/fonts

up:
	docker-compose up -d

down:
	docker-compose down

restart: down up

logs:
	docker-compose logs -f
